<?php
/**
 * Muse Test functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package uyg_theme
 */

if ( ! function_exists( 'uyg_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function uyg_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Muse Test, use a find and replace
		 * to change 'uyg-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'uyg-theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'uyg-theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'uyg_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'uyg_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function uyg_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'uyg_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'uyg_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function uyg_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'uyg-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'uyg-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'uyg_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function uyg_theme_scripts() {
	wp_enqueue_style( 'uyg-theme-style', get_template_directory_uri() . '/styles/style.css' );
	wp_enqueue_script( 'uyg-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '20151215', true );

	wp_enqueue_script( 'uyg-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	// wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js', array(), '20151215', true );
	// wp_enqueue_script( 'selectFx', get_template_directory_uri() . '/js/selectFx.js', array(), '20151215', true );
	// wp_enqueue_script( 'customizer', get_template_directory_uri() . '/js/customizer.js', array(), '20151215', true );
	// wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array(), '20151215', true );
	// wp_enqueue_script( 'full-screen-form', get_template_directory_uri() . '/js/fullscreenForm.js', array(), '20151215', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array(), '20151215', true );
	wp_localize_script( 'scripts', 'admin_ajax', array(
		'ajaxurl'    => admin_url( 'admin-ajax.php' ),
	) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'uyg_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_action( 'wp_ajax_form_action', 'form_action' );
add_action( 'wp_ajax_nopriv_form_action', 'form_action' );
function form_action() {
	
	// check to see if the $_POST variable is empty
	if (!empty($_POST)) :
		$name = '';
		$email = '';
		$password = '';
		$email_content = array();
		$post_content = array();
		// split the $_POST array into key/value pairs
		foreach($_POST as $key => $value) :
			if ($key !== 'action') :
				if ('fullName' == $key) :
					$email_content[] = [
						'question' => "What's your name?",
						'answer' => $value,
					];
					$name = trim($value);
					$password = $name . rand(100, 3000);
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('email'	 == $key) :
					$email_content[] = [
						'question' => "What's your email address?",
						'answer' => $value,
					];
					$email = $value;
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('typePerson' == $key) :
					$email_content[] = [
						'question' => "If your company was a person, What kind of person would it be?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('mascFem' == $key) :
					$email_content[] = [
						'question' => "Are they more Masculine or Feminine?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('voice' == $key) :
					$email_content[] = [
						'question' => "Describe their voice",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('face' == $key) :
					$email_content[] = [
						'question' => "What does their face look like?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('carry' == $key) :
					$email_content[] = [
						'question' => "How do they carry themselves?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('music' == $key) :
					$email_content[] = [
						'question' => "What kind of music do they listen to?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('age' == $key) :
					$email_content[] = [
						'question' => "How old are they?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('matters' == $key) :
					$email_content[] = [
						'question' => "What matters most to them in life?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('party' == $key) :
					$email_content[] = [
						'question' => "If your company was at a party, Who is most likely to engage with them?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('gravitate' == $key) :
					$email_content[] = [
						'question' => "What types of people will gravitate toward them and why?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('traits' == $key) :
					$email_content[] = [
						'question' => "What personality traits will help them to connect with the other partygoers?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('traitsBad' == $key) :
					$email_content[] = [
						'question' => "What traits might put people off?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
				if ('colour' == $key) :
					$email_content[] = [
						'question' => "Which primary colour best describes the personality of your brand?",
						'answer' => $value,
					];
					$post_content[] = [
						'key' => $key,
						'value' => $value
					];
				endif;
			endif;
		endforeach;

		$to = 'brandin@musemarketinggroup.ca';
		$subject = $name . ' is Uncovering Their Greatness';
		$headers = array('Content-Type: text/html; charset=UTF-8','From: Uncover Your Greatness <muse@uncoveryourgreatness.com>');
		$user_id = wp_create_user( $name, $password, $email );
		$post_array = array(
			'post_author' => $user_id,
			'post_title' => 'UYG: ' . $name,
			'post_type' => 'uyg-entries',
			'post_status' => 'publish	'
		);
		$post_id = wp_insert_post($post_array);
		foreach ($post_content as $post) :
			update_field($post['key'], $post['value'], $post_id);
		endforeach;


		$email = uyg_email_template($email_content, get_the_permalink( $post_id ));
		
		wp_mail( $to, $subject, $email, $headers );

		print 'Thank you, lets begin uncovering your greatness.';
	endif;

	wp_die();
}

// input: array
// function loops through array that has been passed and generates an email template
function uyg_email_template($content, $link) {

	$email = '<div style="background-colour:#eee; padding:15px;"><center><table style="width:100%;max-width:800px;">';
	$email .= '<tr><td colspan="2"><h1 style="font-family:Arial">You Are On Your Way to Uncovering Your Greatness</h1></td></tr>';
	foreach ($content as $c) :
		if ($c['question'] == "Which primary colour best describes the personality of your brand?") :
			$email .= '<tr><th style="padding:15px;border:none;font-size:18px;text-align:left;font-family:Arial">'. $c['question'] . '</th><td style="padding:15px;border:none;font-size:18px;font-family:Arial;background-color:'.$c['answer'].'">'. $c['answer'] . '</td></tr>';
		else :
			$email .= '<tr><th style="padding:15px;border:none;font-size:18px;text-align:left;font-family:Arial">'. $c['question'] . '</th><td style="padding:15px;border:none;font-size:18px;font-family:Arial">'. $c['answer'] . '</td></tr>';
		endif;
	endforeach;
	$email .= '<tr><td colspan="2"><a href="'.$link.'" target="_blank">View Your Journey</a></td></tr>';
	$email .= '</table></center></div>';

	return $email;
}

// add entries post type

function wpdocs_codex_book_init() {
	$labels = array(
			'name'                  => _x( 'Entries', 'Post type general name', 'textdomain' ),
			'singular_name'         => _x( 'Entry', 'Post type singular name', 'textdomain' ),
			'menu_name'             => _x( 'Entries', 'Admin Menu text', 'textdomain' ),
			'name_admin_bar'        => _x( 'Entry', 'Add New on Toolbar', 'textdomain' ),
			'add_new'               => __( 'Add New', 'textdomain' ),
			'add_new_item'          => __( 'Add New Entry', 'textdomain' ),
			'new_item'              => __( 'New Entry', 'textdomain' ),
			'edit_item'             => __( 'Edit Entry', 'textdomain' ),
			'view_item'             => __( 'View Entry', 'textdomain' ),
			'all_items'             => __( 'All Entries', 'textdomain' ),
			'search_items'          => __( 'Search Entries', 'textdomain' ),
			'parent_item_colon'     => __( 'ParentEntries:', 'textdomain' ),

	);

	$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'comments' ),
	);

	register_post_type( 'uyg-entries', $args );
}

add_action( 'init', 'wpdocs_codex_book_init' );

// ajax wordpress login:
add_action( 'wp_ajax_uyg_action', 'uygx_action' );
add_action( 'wp_ajax_nopriv_uyg_action', 'uyg_action' );
function uyg_action(){

	// check_ajax_referer( 'ajax-login-nonce', 'security' );
	
	$info = array();

	if (!empty($_POST)) :
		$info['user_login'] = $_POST['email'];
		$info['user_password'] = $_POST['password'];
		$info['remember'] = TRUE;
		$user_signon = wp_signon( $info, false );
		if ( is_wp_error($user_signon) ){
			echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
	} else {
		echo json_encode(array('loggedin'=>true, 'message'=> true));
	}
	endif;

	
	
	wp_die();
	}