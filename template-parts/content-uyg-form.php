<?php

	//variables for questions and input type
	$full_name = get_field_object('field_5dcf1b9bb846a');
	$email = get_field_object('field_5dcf1bc7b846b');
	$type_person = get_field_object('field_5dcf1beab846c');
	$masc_fem = get_field_object('field_5dcf1bfdb846d');
	$voice = get_field_object('field_5dcf1c7eb846e');
	$face = get_field_object('field_5dcf1c92b846f');
	$carry = get_field_object('field_5dcf1c9bb8470');
	$music = get_field_object('field_5dcf1ca8b8471');
	$age = get_field_object('field_5dcf1cb2b8472');	
	$matters = get_field_object('field_5dcf1d38b8473');
	$party = get_field_object('field_5dcf1d46b8474');
	$gravitate = get_field_object('field_5dcf1d52b8475');
	$traits = get_field_object('field_5dcf1d5eb8476');
	$traits_bad = get_field_object('field_5dcf1d85b8477');
	$colour = get_field_object('field_5dcf1d97b8478');
?>
<div id="overlay" class="display-none"></div>
<div class="form-fullscreen" id="formContainer">

			<div class="fs-form-wrap" id="fs-form-wrap">

				<form id="uygForm" class="fs-form fs-form-full" autocomplete="off" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
					<ol class="fs-fields" id="formFields">
						<li class="fs-current">
							<label class="fs-field-label fs-anim-upper" for="fullName"><?php print $full_name['label']; ?></label>
							<input class="fs-anim-lower" id="name" name="fullName" type="<?php print $full_name['type']; ?>" placeholder="<?php print $full_name['placeholder'] ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="email" data-info="We won't send you spam, we promise..."><?php print $email['label']; ?></label>
							<input class="fs-anim-lower" id="email" name="email" type="<?php print $email['type']; ?>" placeholder="<?php print $email['placeholder']; ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="typePerson" data-info=""><?php print $type_person['label']; ?></label>
							<input class="fs-anim-lower" id="typePerson" name="typePerson" placeholder="<?php print $type_person['placeholder']; ?>" type="<?php print $type_person['type']; ?>">
						</li>
						<li data-input-trigger>
							<label class="fs-field-label fs-anim-upper" for="mascFem" data-info=""><?php print $masc_fem['label']; ?></label>
							<div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
								<?php foreach($masc_fem['choices'] as $key => $value) : ?>
								<span><input id="mascFem_<?php print $key; ?>" name="mascFem" type="radio" value="<?php print $key; ?>"/><label for="q4b" class="radio-opaque radio-<?php print $key; ?>"><?php print $value; ?></label></span>
								<?php endforeach; ?>
								
							</div>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="voice" data-info=""><?php print $voice['label']; ?></label>
							<input class="fs-anim-lower" id="voice" name="voice" type="<?php print $voice['type']; ?>" placeholder="<?php print $type['placeholder'] ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="face" data-info=""><?php print $face['label']; ?></label>
							<input class="fs-anim-lower" id="face" name="face" type="<?php print $face['type'] ?>" placeholder="<?php print $face['placeholder']; ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="carry" data-info=""><?php print $carry['label']; ?></label>
							<input class="fs-anim-lower" id="carry" name="carry" type="<?php print $carry['type']; ?>" placeholder="<?php print $carry['placeholder']; ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="music" data-info=""><?php print $music['label']; ?></label>
							<input class="fs-anim-lower" id="music" name="music" type="<?php print $music['type']; ?>" placeholder="<?php print $music['placeholder']; ?>" required/>
						</li>
						<li data-input-trigger>
							<label class="fs-field-label fs-anim-upper" for="age" data-info=""><?php print $age['label']; ?></label>
							<div class="fs-radio-group fs-radio-custom-age age-custom-radio clearfix fs-anim-lower">
								<?php foreach ($age['choices'] as $key => $value) :	?>
											<!-- <span><input id="ageb" name="age" type="<?php print $age['type']; ?>" value="<?php print $key ?>"/><label for="age" class="radio-age"><?php print $value; ?></label></span>
											<label class="container"> -->
										<label for="age" class="container"><?php print $value; ?>
										<input type="radio" name="age" value="<?php print $key;  ?>">
										<span class="checkmark"></span>
									</label>
								<?php endforeach ?>
							</div>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="matters" data-info=""><?php print $matters['label']; ?></label>
							<input class="fs-anim-lower" id="matters" name="matters" type="<?php print $matters['type']; ?>" placeholder="<?php print $matters['placeholder']; ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="party" data-info=""><?php print $party['label']; ?></label>
							<input class="fs-anim-lower" id="party" name="party" type="<?php print $party['type']; ?>" placeholder="<?php print $party['placeholder']; ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="gravitate" data-info=""><?php print $gravitate['label']; ?></label>
							<input class="fs-anim-lower" id="gravitate" name="gravitate" type="<?php print $gravitate['type']; ?>" placeholder="<?php print $gravitate['placeholder']; ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="traits" data-info=""><?php print $traits['label']; ?></label>
							<input class="fs-anim-lower" id="traits" name="traits" type="<?php print $traits['type']; ?>" placeholder="<?php print $traits['placeholder']; ?>" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="traitsBad" data-info=""><?php print $traits_bad['label']; ?></label>
							<input class="fs-anim-lower" id="traitsBad" name="traitsBad" type="<?php print $traits_bad['type']; ?>" placeholder="<?php print $traits_bad['placeholder'] ?>" required/>
						</li>
						<li data-input-trigger>
							<label class="fs-field-label fs-anim-upper" data-info="">Which primary colour best describes the personality of your brand?</label>
							<div class="colour-container">
								<?php foreach ($colour['choices'] as $key => $value) :	 ?>
									<label class="container">
										<input type="radio" name="radio" value="#<?php print $key;  ?>">
										<span class="checkmark" style="background-color:#<?php print $key ?>;"></span>
									</label>
								<?php endforeach; ?>
								</div>
						</li>

					</ol><!-- /fs-fields -->
					<div class="controls">
						<button class="uyg-continue uyg-button" id="continueButton">Continue</button>
						<button class="uyg-back display-none" id="backButton">Go Back</button>
						<button class="uyg-preview uyg-button display-none" id="previewButton">Preview Submission</button>
						<div id="steps">
							<span id="currentStep"></span>
							<span>/</span>
							<span id="totalSteps"></span>
						</div>
					</div>
					<button class="fs-submit display-none uyg-button" type="submit" id="submitButton">Send answers</button>
					<input type="hidden" name="action" value="form_action">
				</form><!-- /fs-form -->
			</div><!-- /fs-form-wrap -->



    </div><!-- /container -->