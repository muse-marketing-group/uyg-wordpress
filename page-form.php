<?php 

// Template Name: UYG Form

get_header('form');
?>
<main id="site-content" class="uyg-page">
<?php
	if (is_user_logged_in()) : ?>
		<section class="uyg-wrapper animated slideInLeft">
		<?php
		get_template_part( 'template-parts/content-uyg-form' );
		?>
		</section>
	<?php else :
		get_template_part( 'inc/login-form');
		?>
		<section class="uyg-wrapper hide-form">
			<?php	get_template_part( 'template-parts/content-uyg-form' ); ?>
		</section>
		<?php
	endif;
	?>
<section class="img-wrap animated slideInRight">
	<?php
	print get_the_post_thumbnail(get_the_id(), 'full');
?>
</section>
</main>

<?php
get_footer('form'); ?>