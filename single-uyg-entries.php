<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Muse_Test
 */

get_header('form');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php

      print '<h1>'.get_the_title().'</h1>';

      print "What's your name?: " . get_field('fullName') . '<br>';
      
      print "What's your email address? " . get_field('email') . '<br>';
      
      print "If your company was a person, What kind of person would it be? " . get_field('typePerson') . '<br>';
    
      print "Are they more Masculine or Feminine? " . get_field('mascFem') . '<br>';
      
      print 'Describe their voice ' . get_field('voice') . '<br>';
     
      print "What does their face look like? " . get_field('face') . '<br>';
      
      print "How do they carry themselves? " . get_field('carry') . '<br>';
     
      print "What kind of music do they listen to? " . get_field('music') . '<br>';
      
      print "How old are they? " . get_field('age') . '<br>';
     
      print "What matters most to them in life? " .get_field('matters') . '<br>';
     
      print "If your company was at a party, Who is most likely to engage with them? " . get_field('party') . '<br>';
      
      print "What types of people will gravitate toward them and why? " . get_field('gravitate') . '<br>';
      
      print "What personality traits will help them to connect with the other partygoers? " . get_field('traits') . '<br>';
      
      print "What traits might put people off? " . get_field('traitsBad') . '<br>';
     
      print "Which primary colour best describes the personality of your brand? " .get_field('colour');
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer('form');
