			// (function() {
			// 	var formWrap = document.getElementById( 'fs-form-wrap' );

			// 	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
			// 		new SelectFx( el, {
			// 			stickyPlaceholder: false,
			// 			onChange: function(val){
			// 				document.querySelector('span.cs-placeholder').style.backgroundColor = val;
			// 			}
			// 		});
			// 	} );

			// 	new FForm( formWrap, {
			// 		onReview : function() {
			// 			classie.add( document.body, 'overview' ); // for demo purposes only
			// 		}
			// 	} );
			// })();

// jQuery()

const formFields = document.getElementById('formFields')
const continueButton = document.getElementById('continueButton')
const backButton = document.getElementById('backButton')
const previewButton = document.getElementById('previewButton')
const submitButton = document.getElementById('submitButton')
const mascFemInputs = document.querySelectorAll('.radio-opaque')
const ageInputs = document.querySelectorAll('.radio-age')
const currentStep = document.querySelector('#currentStep')
const totalSteps = document.querySelector('#totalSteps')
const controls = document.querySelector('.controls')
let steps = 1;
// get and set the total number of steps
totalSteps.innerHTML = formFields.children.length
// set initial step
currentStep.innerHTML = steps

continueButton.addEventListener('click', function(e){
	e.preventDefault()
	const nextFormElement = document.querySelectorAll('.fs-current')[0].nextElementSibling;
	const currentElement = document.querySelectorAll('.fs-current')[0];
	console.log(currentElement.children[1].value)
	if (currentElement.children[1].value !== '') {
	backButton.classList.remove('display-none')
	if (nextFormElement !== null) {
		currentElement.classList.remove('fs-current')
		controls.classList.remove('animated', 'slideInLeft')
		controls.classList.remove('animated', 'slideInRight')
		nextFormElement.classList.add('fs-current', 'animated', 'slideInRight')
		controls.classList.add('animated', 'slideInLeft')
		currentStep.innerHTML = ++steps
	} 
}
	if (nextFormElement.nextElementSibling == null) {
		previewButton.classList.remove('display-none')
		continueButton.classList.add('display-none')
	}
})

backButton.addEventListener('click', function(e){
	e.preventDefault()
	const previousFormElement = document.querySelectorAll('.fs-current')[0].previousElementSibling;
	const currentElement = document.querySelectorAll('.fs-current')[0];
	document.querySelector('#uygForm').classList.remove('position-absolute')
	submitButton.classList.add('display-none')
	if (previousFormElement !== null) {
		currentElement.classList.remove('fs-current')
		previousFormElement.classList.add('fs-current')
		submitButton.classList.add('display-none')
		previewButton.classList.add('display-none')
		continueButton.classList.remove('display-none')
		formFields.classList.remove('form-overview')
		// animation
		previousFormElement.classList.remove('animated', 'slideInRight')
		controls.classList.remove('animated', 'slideInLeft')
		controls.classList.remove('animated', 'slideInRight')
		previousFormElement.classList.add('fs-current', 'animated', 'slideInLeft')
		controls.classList.add('animated', 'slideInRight' )
		currentStep.innerHTML = --steps
	}
	if (previousFormElement.previousElementSibling == null) {
		backButton.classList.add('display-none')
	}
	if (nextFormElement.nextElementSibling == null) {
		previewButton.classList.remove('display-none')
		continueButton.classList.add('display-none')
	}
})

previewButton.addEventListener('click', function(e){
	e.preventDefault()
	formFields.classList.add('form-overview')
	submitButton.classList.remove('display-none')
	document.querySelector('#formContainer').classList.add('position-absolute-overview')
	document.querySelector('#uygForm').height = '100vh'
	document.querySelector('#uygForm').classList.add('position-absolute')
	this.classList.add('display-none')
})

// add opacity class to input
mascFemInputs.forEach(el => el.addEventListener('click', function() {
	mascFemInputs.forEach(el => el.classList.remove('opaque-input-selected'))
	el.classList.add('opaque-input-selected')
}) 

)
ageInputs.forEach(el => el.addEventListener('click', function() {
	ageInputs.forEach(el => el.classList.remove('opaque-input-selected'))
	el.classList.add('opaque-input-selected')
}) 

)
jQuery(document).ready(function($) {
	
	$('#uygForm').on('submit', function(e) {
		e.preventDefault();
		const ajax = admin_ajax.ajaxurl;
		console.log(ajax);
		const formData = $('#uygForm').serializeArray();
		console.log(formData);
		$.ajax({
			url: ajax, 
			type: 'POST',
			data: formData,
			action: 'form_action',
			success: function( response){
				console.log(response);
				$('#formContainer').empty().append(`<div style="text-align:center;margin-top:200px;"><p>${response}</p></div>`);
			},
			error: function(error){
				console.log("error");
			}
		});
	});
	$('#loginForm').on('submit', function(e) {
		e.preventDefault();
		
			//login form
			console.log(this);
			const ajax = admin_ajax.ajaxurl;
			const formData = $('#loginForm').serializeArray();
			console.log(formData)
			console.log(ajax)
			$.ajax({
				url: ajax,
				type: 'POST',
				data: formData,
				action: 'uyg_action',
				success: function(data){
					console.log(data)
					if (data) {
						$('.login-form').addClass('hide-form')
						$('.uyg-wrapper').removeClass('hide-form')
					}
				},
				error: function(error){
					console.log("error");
				}
		});
	});
});
 