
<div class="login-form animated fadeInDown">
  <h1>Login</h1>
  <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" id="loginForm">
  <p>
    <label for="email">Email: 
      <input type="text" name="email">
    </label>
  </p>
  <p>
    <label for="password">Password: 
      <input type="password" name="password">
    </label>
    </p>
    <p class="error-message"></p>
    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
    <input type="hidden" name="action" value="uyg_action">
    <button type="submit" class="uyg-button">Log In</button>
  </form>
</div>